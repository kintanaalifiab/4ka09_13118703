<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tampilan Biodata Mahasiswa</title>
        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <?php
    if(isset($_POST['submit'])){
    $nama=$_POST["nama"];
    $alamat=$_POST["alamat"];
    $tgl=$_POST["tgl"];
    $jk=$_POST["jk"];
    $agama=$_POST["agama"];
    }
    else{
        echo "submit tidak memiliki nilai";
    }
?>
        <header style="background-color: #9683FA;" class="masthead text-white text-center">
            <div class="container d-flex align-items-center flex-column">
                <h1 class="masthead-heading text-uppercase mb-0">John Doe</h1>
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <p class="masthead-subheading font-weight-light mb-0">Tugas Menampilkan data dari form inputan menggunakan PHP</p>
            </div>
        </header>
        <section class="page-section portfolio" id="portfolio">
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-1">Tampilan Biodata Mahasiswa</h2><br><br>
                <div class="container col-lg-6">
                <table class="table table-striped table-hover">
                  <thead style="background-color: #9683FA;">
                  <tr>
                        <td colspan="2" class="text-light rounded-3" align="center"><b>Biodata Mahasiswa</b></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td>Nama</td>
                        <td><?php echo $nama; ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td><?php echo $alamat; ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir</td>
                        <td><?php echo $tgl; ?></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td><?php echo $jk; ?></td>
                    </tr>
                    <tr>
                        <td>Agama</td>
                        <td><?php echo $agama; ?></td>
                    </tr>
                  </tbody>
                </table>
                <button type="button" class="btn btn-primary" onclick="history.back();">Back</button>
                </div>
              
        </section>
    </body>
</html>
